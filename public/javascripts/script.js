// create the module and name it scotchApp
var scotchApp = angular.module('customerBills', ['ngRoute']);

// configure our routes
scotchApp.config(function($routeProvider) {
    $routeProvider

    // route for the home page
        .when('/', {
            templateUrl : './pages/grid.html',
            controller  : 'mainController'
        })

        .otherwise({
            redirectTo :'/'
        });

});

// create the controller and inject Angular's $scope
scotchApp.controller('mainController', function($scope, $http) {

    // Simple GET request example:
    $http.get('http://localhost:3000/add_user_data').then(successCallback, errorCallback);
    function successCallback(response){
        console.log(response)
        $scope.rows=response.data
    }
    function errorCallback(response){

    }
    //$scope.rows = [{name:'abc',age:20,class:'10th'},{name:'test',age:20,class:'10th'},{name:'jj',age:20,class:'10th'},{name:'abc',age:20,class:'10th'}];

});
