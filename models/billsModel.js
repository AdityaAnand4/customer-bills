var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');


var schema = mongoose.Schema


autoIncrement.initialize(mongoose.connection, function(err) {
    if (err) {
        console.log('Unable to connect to Mongo.     '+err)
    }
});

var billsSchema= new schema({

    billDate:Date,
    items:[
        {
        name:String,
        quantity:Number,
        rate:Number
        }
    ],
    discount:Number,
    tax :Number,
    customerId:{type: mongoose.Schema.Types.ObjectId, ref: 'customer'},

})

billsSchema.plugin(autoIncrement.plugin, {model :'bills','field':'billNumber'})
module.exports = mongoose.model('bills', billsSchema)