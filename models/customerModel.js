var mongoose = require('mongoose');


var schema = mongoose.Schema
var customerSchema=new schema({
    name : String,
    mobile : String,
    phone : String,
    addresses : [
        {
            flat : String,
            street : String,
            pinCode : String
        }
    ],
    dob : Date,
    email:String,
    bills:[{type: mongoose.Schema.Types.ObjectId, ref: 'bills'}],
})

module.exports = mongoose.model('customer', customerSchema);