var express = require('express');
var router = express.Router();

var customerModel=require('./../models/customerModel')
var billsmodel=require('./../models/billsModel')


router.post('/', function(req, res, next) {
var users=[
    {
        name:'Nitin',
        mobile:'9999876555',
        phone:'111211112',
        addresses:[{
            flat:'123',
            street:'456',
            state:'delhi',
            pinCode:'110054'
        }],
        dob:'2015-04-03',
        email:'nitin@gmail.com'
    },
    {
        name:'Adi',
        mobile:'9999856999',
        phone:'11121221122',
        addresses:[{
            flat:'1234',
            street:'4567',
            state:'Mumbai',
            pinCode:'110053'
        }],
        dob:'2015-04-04',
        email:'adi@gmail.com'
    },
    {
        name:'Rohit',
        mobile:'9999876535',
        phone:'1234567890',
        addresses:[{
            flat:'123332',
            street:'456222',
            state:'Raipur',
            pinCode:'110055'
        }],
        dob:'2011-05-01',
        email:'rohit@gmail.com'
    },
    {
        name:'Arun',
        mobile:'9711247555',
        phone:'114567890',
        addresses:[{
            flat:'121',
            street:'426',
            state:'delhi',
            pinCode:'110004'
        }],
        dob:'1992-01-01',
        email:'arun@gmail.com'
    }
]
customerModel.collection.insert(users,function(err,resultInsert){
    if (err) {
        console.log(err)
        // TODO: handle error
    } else {
        customerModel.find(function(err,resultData){
            console.log('no of users sucessfully stored were.',resultData.length);
            res.send(JSON.stringify({response:resultData}))
        })

    }
})

})


router.get('/',function(req,res,next){
customerModel.find().populate('bills').exec(function(err,resultCustomerData){
    if(err)
        console.log(err)
    console.log(JSON.stringify(resultCustomerData))
    var objToSend=[]
    var resultCustomerDataLength=resultCustomerData.length;
    for(var i=0;i<resultCustomerDataLength;i++){
        var obj={}
        obj.customer_name= resultCustomerData[i].name
        console.log(resultCustomerData[i].mobile)
        obj.mobile= resultCustomerData[i].mobile
        obj.phone= resultCustomerData[i].phone
        obj.email= resultCustomerData[i].email
        obj.noOfBills= resultCustomerData[i].bills.length;
        var amount=0;
        var discount=0;
        var tax=0;
        for(var j=0;j<resultCustomerData[i].bills.length;j++){

            var itemsLength=resultCustomerData[i].bills[j].items.length

            for(var k=0;k<itemsLength;k++){
                amount+=parseInt(resultCustomerData[i].bills[j].items[k].rate) * parseInt(resultCustomerData[i].bills[j].items[k].quantity)
                discount+= parseInt(resultCustomerData[i].bills[j].items[k].rate) * parseInt(resultCustomerData[i].bills[j].items[k].quantity)*resultCustomerData[i].bills[j].discount /100
                tax+=  discount*(resultCustomerData[i].bills[j].tax)/100
            }
        }
        console.log(amount)
        console.log("disc",discount)
        amount=amount-discount+tax
        obj.amount=amount;
        obj.avgAmount=amount/parseInt(resultCustomerData[i].bills.length);
        console.log(amount)
        objToSend.push(obj);
    }
    res.send(JSON.stringify(objToSend))
})

})
module.exports = router;
