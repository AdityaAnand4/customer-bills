# README #

This README will guide you through the features and setup process

### Customer-bills calculator ###

* This application is a MEAN application, it has 2 scripts to generate user and bills data and a angular view to display the data

### Things for setup ###

* MongoDb
* Nodejs

### Process to set up ###

* Clone this repo 
* Run "sudo npm install" 
* Navigate to bin/www and run "sudo node www"
* Make sure Mongo server is up and running on localhost on port 27017

### How to start ###

* Navigate to "localhost:3000/" on your browser
* run the customer script first
* run the bills script second
* then click on the 'load table' submit button